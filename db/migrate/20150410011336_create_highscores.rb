class CreateHighscores < ActiveRecord::Migration
  def change
    create_table :highscores do |t|
      t.references :user, index: true
      t.integer :score
      t.integer :level

      t.timestamps null: false
    end
    add_foreign_key :highscores, :users
  end
end
