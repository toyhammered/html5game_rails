# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'
=begin
MyProject::Application.configure do
  config.sass.preferred_syntax = :sass
  config.sass.line_comments = false
  config.sass.cache = false
end
=end
# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

# Rails.application.config.assets.precompile += %w( game/main.css )
# Rails.application.config.assets.precompile += %w( game/mobile.css )
# Rails.application.config.assets.precompile += %w( game/fontfaces.css )

Rails.application.config.assets.precompile += %w( home.css )