class HomeController < ApplicationController
  def index
  	if !logged_in?
			redirect_to splash_url
		end
  end

  def splash
  	render layout: false
  	
  end
end
