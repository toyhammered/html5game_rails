class ApplicationMailer < ActionMailer::Base
  default from: "from@rassiner.com"
  layout 'mailer'
end
