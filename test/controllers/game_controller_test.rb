require 'test_helper'

class GameControllerTest < ActionController::TestCase

	test "should get game" do
    get :show
    assert_response :success
    assert_select "title", "RWBY Gems"
  end


end
