require 'test_helper'

class HighscoreTest < ActiveSupport::TestCase
	def setup
		@user = users(:toy)
		@highscore = @user.highscores.build(score: 12345, level: 3) # check if this is how you add numbers
	end

	test "user id should be present" do
		@highscore.user_id = nil
		assert_not @highscore.valid?
	end

	
#	test "best score should be on top" do
#		assert_equal Highscore.first, highscores(:best_score)
#	end

end
